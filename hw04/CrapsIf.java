/*---------------------------------------------------------|
 * Quinn Robertson                                         |
 * CSE 002                                                 |
 * Homework 04                                             |
 * The game of craps but done with if                      |
 * statements                                              |
 * --------------------------------------------------------|
 */
import java.util.Scanner;
import java.util.Random;
public class CrapsIf{
  // Creating main method
  public static void main(String[] args){
    // Declaring variables
    int dieOne = 0;
    int dieTwo = 0;
    String diceString = "";
    boolean randomChoice = true;
    int randChoiceDeterminer;
    Random randGen = new Random();
    Scanner scnr = new Scanner( System.in );
    
    // Prompting for user input or not
    System.out.println("Do you want to use random numbers? (0 for no, 1 for yes, defaults to random)");
    randChoiceDeterminer = scnr.nextInt();
    
    // Switching value of whether or not it is random
    if (randChoiceDeterminer == 1){
        randomChoice = true;
    }
    else if (randChoiceDeterminer == 0){
        randomChoice = false;
    }
    else{
        randomChoice = true;
    }
    
    // Determining the numbers, by choice or random
    if (randomChoice){
        dieOne = randGen.nextInt(6)+1;
        dieTwo = randGen.nextInt(6)+1;
    }
    else {
      // Determining while the numbers might end up out of range
      while (!(((dieOne <= 6) && (dieOne >= 1)) && ((dieTwo <= 6) && (dieTwo >= 1)))){
        System.out.println("Input value of die one");
        dieOne = scnr.nextInt();
        System.out.println("Input value of die two");
        dieTwo = scnr.nextInt();
      }
    }
    
    // Determining the string value of the dice
    if (dieOne == 1){
      if (dieTwo == 1){diceString = "Snake Eyes";}
      else if (dieTwo == 2){diceString = "Ace Deuce";}
      else if (dieTwo == 3){diceString = "Easy Four";}
      else if (dieTwo == 4){diceString = "Fever Five";}
      else if (dieTwo == 5){diceString = "Easy Six";}
      else if (dieTwo == 6){diceString = "Seven Out";}
    }
    else if (dieOne == 2){
      if (dieTwo == 1){diceString = "Ace Deuce";}
      else if (dieTwo == 2){diceString = "Hard Four";}
      else if (dieTwo == 3){diceString = "Fever Five";}
      else if (dieTwo == 4){diceString = "Easy Six";}
      else if (dieTwo == 5){diceString = "Seven Out";}
      else if (dieTwo == 6){diceString = "Easy Eight";}
    }
    else if (dieOne == 3){
      if (dieTwo == 1){diceString = "Easy Four";}
      else if (dieTwo == 2){diceString = "Fever Five";}
      else if (dieTwo == 3){diceString = "Hard Six";}
      else if (dieTwo == 4){diceString = "Seven Out";}
      else if (dieTwo == 5){diceString = "Easy Eight";}
      else if (dieTwo == 6){diceString = "Nine";}
    }
    else if (dieOne == 4){
      if (dieTwo == 1){diceString = "Fever Five";}
      else if (dieTwo == 2){diceString = "Easy Six";}
      else if (dieTwo == 3){diceString = "Seven Out";}
      else if (dieTwo == 4){diceString = "Easy Eight";}
      else if (dieTwo == 5){diceString = "Nine";}
      else if (dieTwo == 6){diceString = "Easy Ten";}
    }
    else if (dieOne == 5){
      if (dieTwo == 1){diceString = "Easy Six";}
      else if (dieTwo == 2){diceString = "Seven Out";}
      else if (dieTwo == 3){diceString = "Easy Eight";}
      else if (dieTwo == 4){diceString = "Nine";}
      else if (dieTwo == 5){diceString = "Hard Ten";}
      else if (dieTwo == 6){diceString = "Yo-leven";}
    }
    else {
      if (dieTwo == 1){diceString = "Seven Out";}
      else if (dieTwo == 2){diceString = "Easy Eight";}
      else if (dieTwo == 3){diceString = "Nine";}
      else if (dieTwo == 4){diceString = "Easy Ten";}
      else if (dieTwo == 5){diceString = "Yo-leven";}
      else if (dieTwo == 6){diceString = "Boxcars";}
    }
    // Outputting the value of the string
    System.out.println("The slang of these dice is " + diceString);
  } // End of main method
} // End of class