/*---------------------------------------------------------|
 * Quinn Robertson                                         |
 * CSE 002                                                 |
 * Homework 04                                             |
 * The game of craps but done with switches                |
 * statements                                              |
 * --------------------------------------------------------|
 */
import java.util.Random;
import java.util.Scanner;
public class CrapsSwitch{
  // Creating main method
  public static void main(String[] args){
    // Declaring variables
    int dieOne = 0;
    int dieTwo = 0;
    String diceString = "";
    int randomChoice = 1;
    String randChoiceDeterminer;
    Random randGen = new Random();
    Scanner scnr = new Scanner( System.in );
    
    // Prompting user to determine if they want to use random numbers
    System.out.println("Do you want to use random numbers? (y/n, defaults to random)");
    randChoiceDeterminer = scnr.nextLine();
    
    // Switching value of whether or not it is random
    switch (randChoiceDeterminer){
      case "y":
        randomChoice = 1;
        break;
      case "n":
        randomChoice = 0;
        break;
      default:
        randomChoice = 1;
        break;
    }
    
    // Determining the numbers, by choice or random
    switch (randomChoice){
      case 1:
        dieOne = randGen.nextInt(6)+1;
        dieTwo = randGen.nextInt(6)+1;
        break;
      case 0:
        // Keeping values in ranges
        while (!(((dieOne <= 6) && (dieOne >= 1)) && ((dieTwo <= 6) && (dieTwo >= 1)))){
          System.out.println("Input value of die one");
          dieOne = scnr.nextInt();
          System.out.println("Input value of die two");
          dieTwo = scnr.nextInt();
        }
        break;
    }
    
    // Determining the string value of the dice
    switch(dieOne){
      case 1:
        switch(dieTwo){
          case 1: diceString = "Snake Eyes";
            break;
          case 2: diceString = "Ace Deuce";
            break;
          case 3: diceString = "Easy Four";
            break;
          case 4: diceString = "Fever Five";
            break;
          case 5: diceString = "Easy Six";
            break;
          case 6: diceString = "Seven Out";
            break;
        }
        break;
      case 2:
        switch(dieTwo){
          case 1: diceString = "Ace Deuce";
            break;
          case 2: diceString = "Hard Four";
            break;
          case 3: diceString = "Fever Five";
            break;
          case 4: diceString = "Easy Six";
            break;
          case 5: diceString = "Seven Out";
            break;
          case 6: diceString = "Easy Eight";
            break;
        }
        break;
      case 3:
        switch(dieTwo){
          case 1: diceString = "Easy Four";
            break;
          case 2: diceString = "Fever Five";
            break;
          case 3: diceString = "Hard Six";
            break;
          case 4: diceString = "Seven Out";
            break;
          case 5: diceString = "Easy Eight";
            break;
          case 6: diceString = "Nine";
            break;
        }
        break;
      case 4:
        switch(dieTwo){
          case 1: diceString = "Fever Five";
            break;
          case 2: diceString = "Easy Six";
            break;
          case 3: diceString = "Seven Out";
            break;
          case 4: diceString = "Easy Eight";
            break;
          case 5: diceString = "Nine";
            break;
          case 6: diceString = "Easy Ten";
            break;
        }
        break;
      case 5:
        switch(dieTwo){
          case 1: diceString = "Easy Six";
            break;
          case 2: diceString = "Seven Out";
            break;
          case 3: diceString = "Easy Eight";
            break;
          case 4: diceString = "Nine";
            break;
          case 5: diceString = "Hard Ten";
            break;
          case 6: diceString = "Yo-leven";
            break;
        }
        break;
      case 6:
        switch(dieTwo){
          case 1: diceString = "Seven Out";
            break;
          case 2: diceString = "Easy Eight";
            break;
          case 3: diceString = "Nine";
            break;
          case 4: diceString = "Easy Ten";
            break;
          case 5: diceString = "Yo-leven";
            break;
          case 6: diceString = "Boxcars";
            break;
        }
        break;
    
    }
    
    // Outputting the value of the string
    System.out.println("The slang of these dice is " + diceString);
  } // End of main method
} // End of class