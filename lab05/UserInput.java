/*--------------------------------------------
 * Quinn Robertson
 * CSE 002
 * Lab 05
 * User input used to determine
 * course information
 */
import java.util.Scanner;

public class UserInput{
  // Main method
  public static void main(String[] args){
    // Declaring variables
    int classNumber = 0;
    String deptName = "";
    int numberOfTimesAWeek = 0;
    int classHourStartTime = 0, classMinuteStartTime = (0-1);
    String instructorFirstName = "", instructorLastName = "";
    int numberOfStudents = 0;
    
    // Creating scanner
    Scanner scnr = new Scanner( System.in );
    
    // Gathering input in infinite loops
    // Asking for course number
    while(classNumber == 0){
      System.out.print("Please enter a course number: ");
      // Creating bool for checking for int
      boolean hasInt = scnr.hasNextInt();
      if (hasInt){
        classNumber = scnr.nextInt();
      }
      else{
        scnr.next();
        System.out.println("You entered a non-integer, please re-enter");
      }
    }
    
    // Asking for course dept
    while(deptName.equals("")){
      System.out.print("Please enter a course department: ");
      // Creating bool for checking for int
      boolean hasInt = scnr.hasNextInt();
      if (!hasInt){
        deptName = scnr.next();
      }
      else{
        scnr.next();
        System.out.println("You entered a non-string, please re-enter");
      }
    }
    
    // Asking number of times a week
    while(numberOfTimesAWeek == 0){
      System.out.print("Please enter how many times the course occurs a week: ");
      // Creating bool for checking for int
      boolean hasInt = scnr.hasNextInt();
      if (hasInt){
        numberOfTimesAWeek = scnr.nextInt();
        if(numberOfTimesAWeek > 5){
          numberOfTimesAWeek = 0;
          System.out.println("You entered something outside the range, please re-enter");
        }
      }
      else{
        scnr.next();
        System.out.println("You entered a non-integer, please re-enter");
      }
    }
    
    // Asking for start times
    // Asking for hour
    while(classHourStartTime == 0){
      System.out.print("Please enter the hour the class starts: ");
      // Creating bool for checking for int
      boolean hasInt = scnr.hasNextInt();
      if (hasInt){
        classHourStartTime = scnr.nextInt();
        if(classHourStartTime > 12){
          classHourStartTime = 0;
          System.out.println("You entered something outside the range, please re-enter");
        }
      }
      else{
        scnr.next();
        System.out.println("You entered a non-integer, please re-enter");
      }
    }
    
    // Asking for minutes
    while(classMinuteStartTime < 0){
      System.out.print("Please enter the minutes the class starts: ");
      // Creating bool for checking for int
      boolean hasInt = scnr.hasNextInt();
      if (hasInt){
        classMinuteStartTime = scnr.nextInt();
        if(classMinuteStartTime > 59){
          classMinuteStartTime = (0-1);
          System.out.println("You entered something outside the range, please re-enter");
        }
      }
      else{
        scnr.next();
        System.out.println("You entered a non-integer, please re-enter");
      }
    }
    
    // Asking for instructor name
    // First name
    // Declaring sentinal
    boolean sentinalFirst = instructorFirstName.equals("");
    while(sentinalFirst){
      System.out.print("Please enter instructor first name: ");
      // Creating bool for checking for int
      boolean hasInt = scnr.hasNextInt();
      if (!hasInt){
        deptName = scnr.next();
        sentinalFirst = false;
      }
      else{
        scnr.next();
        System.out.println("You entered a non-string, please re-enter");
      }
    }
    
    // Last name
    // Declaring sentinal
    boolean sentinalLast = instructorLastName.equals("");
    while(sentinalLast){
      System.out.print("Please enter instructor last name: ");
      // Creating bool for checking for int
      boolean hasInt = scnr.hasNextInt();
      if (!hasInt){
        deptName = scnr.next();
        sentinalLast = false;
      }
      else{
        scnr.next();
        System.out.println("You entered a non-string, please re-enter");
      }
    }
    
    // Asking for number of students
    while(numberOfStudents == 0){
      System.out.print("Please enter number of students: ");
      // Creating bool for checking for int
      boolean hasInt = scnr.hasNextInt();
      if (hasInt){
        numberOfStudents = scnr.nextInt();
      }
      else{
        scnr.next();
        System.out.println("You entered a non-integer, please re-enter");
      }
    }
    
    System.out.println("Class info:\n Course Number: "+classNumber+"\n Course Dept: "+deptName+"\n Number of Meets a Week: "+numberOfTimesAWeek+"\n Start Time: "+classHourStartTime+":"+classMinuteStartTime+"\n Instructor Name: "+instructorFirstName+" "+instructorLastName+"\n Number of Students: "+numberOfStudents);
  }
}