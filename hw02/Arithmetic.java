/*---------------------------------------------------------|
 * Quinn Robertson                                         |
 * CSE 002                                                 |
 * Homework 02                                                  |
 * Arithmetic calculations to calculate final totals       |
 * from store purchases of various items                   |
 * --------------------------------------------------------|
 */
public class Arithmetic {
  // Main method for java program
  public static void main(String[] args){
    // Declaring input variables
    // Number of pairs of pants
    int numPants = 3;
    // Cost per pair of pants
    double pantsPrice = 34.98;
    
    // Number of sweatshirts
    int numShirts = 2;
    // Cost per shirt
    double shirtPrice = 24.99;
    
    // Number of belts
    int numBelts = 1;
    // Cost per belt
    double beltCost = 33.99;
    
    // The tax rate in Pennsylvania
    double paSalesTax = 0.06;
    
    /* Creating variables for the final values */
    // Totals for each item
    double totalPantsCost, totalShirtsCost, totalBeltsCost;
    // Total for taxes of each item
    double totalPantsTax, totalShirtsTax, totalBeltsTax;
    // Overall total sales (without tax), overall tax, total final price (with tax), respectively
    double totalSalesNoTax, totalTax, totalPrice;
    
    // Calculating total price without tax for each item with full precision (stays at 2 decimal places, doesnt need to be changed)
    totalPantsCost = numPants * pantsPrice;
    totalShirtsCost = numShirts * shirtPrice;
    totalBeltsCost = numBelts * beltCost;
    
    // Calculating total tax on each item, with full precision
    totalPantsTax = paSalesTax * totalPantsCost;
    totalShirtsTax = paSalesTax * totalShirtsCost;
    totalBeltsTax = paSalesTax * totalBeltsCost;
    
    // Calculating overall totals with full precision
    totalSalesNoTax = totalPantsCost + totalShirtsCost + totalBeltsCost;
    totalTax = totalPantsTax + totalShirtsTax + totalBeltsTax;
    totalPrice = totalSalesNoTax + totalTax;
    
    /* Truncating the results */
    // Declaring temporary int variables
    // Total for taxes of each item, times 100 and ints
    int hundredTotalPantsTax, hundredTotalShirtsTax, hundredTotalBeltsTax;
    // Overall total sales (without tax), overall tax, total final price (with tax), respectively. Values are 100 times previous and ints
    int hundredTotalSalesNoTax, hundredTotalTax, hundredTotalPrice;
    
    // Giving values to the created variables
    hundredTotalPantsTax = (int) (totalPantsTax * 100);
    hundredTotalShirtsTax = (int) (totalShirtsTax * 100);
    hundredTotalBeltsTax = (int) (totalBeltsTax * 100);
    
    hundredTotalSalesNoTax = (int) (totalSalesNoTax * 100);
    hundredTotalTax = (int) (totalTax * 100);
    hundredTotalPrice = (int) (totalPrice * 100);
    
    // Re-assigning the values for values that needed to be truncated
    totalPantsTax = hundredTotalPantsTax / 100.0;
    totalShirtsTax = hundredTotalShirtsTax / 100.0;
    totalBeltsTax = hundredTotalBeltsTax / 100.0;
    
    totalSalesNoTax = hundredTotalSalesNoTax / 100.0;
    totalTax = hundredTotalTax / 100.0;
    totalPrice = hundredTotalPrice / 100.0;
    
    // Printing findings to screen
    System.out.println("Total price of all pants: "+totalPantsCost);
    System.out.println("Total price of all shirts: "+totalShirtsCost);
    System.out.println("Total price of all belts: "+totalBeltsCost);
    System.out.println('\n');
    System.out.println("Total tax on pants: "+totalPantsTax);
    System.out.println("Total tax on shirts: "+totalShirtsTax);
    System.out.println("Total tax on belts: "+totalBeltsTax);
    System.out.println('\n');
    System.out.println("Total price without tax: "+totalSalesNoTax);
    System.out.println("Total taxes: "+totalTax);
    System.out.println('\n');
    System.out.println("Final total: "+totalPrice);

  }
  
}