/*---------------------
 * Quinn Robertson    |
 * Homework 07        |
 * Methods and        |
 * string manipulation|
 *---------------------
 */
import java.util.Scanner;
public class Hw07{
  // Sample Text
  public static String sampleText(Scanner scnr){
    // Declaring variable
    String text;
    // Prompting user
    System.out.println("Please input text: ");
    text = scnr.nextLine();
    // Printing back text
    System.out.println("You entered: " + text);
    return text;
  }

  // Print Menu
  public static char printMenu(Scanner scnr){
    // Declaring variables
    char input = ' ';

    // Printing prompt and gathering reply
    while(input == ' '){
      System.out.println("MENU" + "\n" + "----------------------\n" +"c - Number of non-whitespace characters" + "\n" + "w - Number of words" + "\n" + "f - Find text" + "\n" + "r - Replace all !'s" + "\n" + "s - Shorten spaces" + "\n" + "q - Quit" + "\n\n" + "Choose an option:");
      input = scnr.next().charAt(0);
      if(input == 'c' || input == 'w' || input == 'f' || input == 'r' || input == 's' || input == 'q'){
        return input;
      }
      else{
        input = ' ';
        System.out.println("Please enter an option.");
      }
    }
    return 'e';
  }

  // Number of non-whitespace characters
  public static int gewtNumOfNonWSCharacters(String str){
    // Declaring int
    int counter = 0;

    // Counting
    for(int i = 0; i <= str.length()-1; ++i){
      if(str.charAt(i) != ' '){
        ++counter;
      }
    }
    return counter;
  }

  // Number of words
  public static int getNumOfWords(String str){
    // Declaring
    int counter = 0;

    // Counting the number of words
    for(int i = 0; i <= str.length()-1; ++i){
      // Conditions skip over multiple spaces
      if(i != 0 && (str.charAt(i) == ' ') && str.charAt(i+1) != ' '){
        ++counter;
      }
    }

    return counter;
  }

  // Number of specific words
  public static int findText(Scanner scnr, String str){
    // Declaring
    String input = "";
    int numOfSpecificWord = 0;

    // Prompting
    System.out.print("Enter a word: ");
    System.out.println();
    input = scnr.next();

    for(int i = 0; i <= str.length()-1; ++i){
      int counter = 0;
      // Conditions make it so the specfic word is present, so that if the use types "owl", howling does not trigger it
      if(str.charAt(i) == input.charAt(0) && ((str.charAt(i+input.length()) == ' ')||(str.length() == i+input.length())) && ((i == 0)||(str.charAt(i-1) == ' '))){
        for(int k = 0; k <= input.length()-1; ++k){
          if(str.charAt(i+k) == input.charAt(k)){
            ++counter;
          }
        }
        if(counter == input.length()){
          ++numOfSpecificWord;
        }
      }
    }
    return numOfSpecificWord;
  }

  // Replacing exclamation points
  public static String replaceExclamation(String str){
    // Declaring
    String finalString = str;

    // Replacing
    for(int i = 0; i <= str.length()-1; ++i){
      if(str.charAt(i) == '!'){
        String tempString = "";
        for(int k = 0; k <= i-1; ++k){
          tempString += str.charAt(k);
        }
        tempString += ".";
        for(int k = i+1; k <= str.length()-1; ++k){
          tempString += str.charAt(k);
        }
        finalString = tempString;
      }
    }
    return finalString;
  }

  // Shorten spaces

  public static String shortenSpace(String str){
    
    // Replacing
    for(int i = 0; i <= str.length()-1; ++i){
      if(str.charAt(i) == ' '){
        if (str.charAt(i+1) == ' '){
          String tempString = "";
          for(int k = 0; k <= i; ++k){
            tempString += str.charAt(k);
          }
          for(int k = i+2; k <= str.length()-1; ++k){
            tempString += str.charAt(k);
          }
          --i; // To check really long spaces
          str = tempString;
        }
        else {continue;}
      }
    }
    return str;
  }

  public static void main(String[] args){
    // Declaring
    Scanner scnr = new Scanner( System.in );
    String text, finalText;
    int numChar, numWords, numSpecificWord;
    char choice;

    // Prompting and calling
    text = sampleText(scnr);
    choice = printMenu(scnr);

    // Going through choices
    if(choice == 'c'){
      numChar = gewtNumOfNonWSCharacters(text);
      System.out.println("Number of non-whitespace characters: " + numChar);
    }
    else if(choice == 'w'){
      numWords = getNumOfWords(text);
      System.out.println("Number of words: " + numWords);
    }
    else if(choice == 'f'){
      numSpecificWord = findText(scnr, text);
      System.out.println("Instances of given word: "+ numSpecificWord);
    }
    else if(choice == 'r'){
      finalText = replaceExclamation(text);
      System.out.println("Edited text: " + finalText);
    }
    else if(choice == 's'){
      finalText = shortenSpace(text);
      System.out.println("Edited text: " + finalText);
    }
    else{
      System.out.println("Quitting...");
    }
  }
}