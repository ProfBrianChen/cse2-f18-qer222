/*---------------------------------------------------------|
 * Quinn Robertson                                         |
 * CSE 002                                                 |
 * Homework 03                                             |
 * Arithmetic calculations to calculate volume of          |
 * a pyramid from the user input                           |
 * --------------------------------------------------------|
 */
import java.util.Scanner;

public class Pyramid{
  // Creating main method
  public static void main(String[] args){
    // Creating scanner
    Scanner scnr = new Scanner( System.in );
    // Creating variables
    double baseSideLength;
    double height;
    double baseArea;
    double volume;
    
    // Prompting and getting input
    System.out.print("What is the side length of the base: ");
    baseSideLength = scnr.nextDouble();
    System.out.print("What is the height of the pyramid: ");
    height = scnr.nextDouble();
    
    // Calculations
    baseArea = Math.pow(baseSideLength, 2.0);
    volume = ((baseArea * height) / 3.0);
    
    // Printing output
    System.out.println("Volume of the pyramid is " + volume);
    
  } // End of main method
} // End of class