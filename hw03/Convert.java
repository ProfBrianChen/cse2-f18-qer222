/*---------------------------------------------------------|
 * Quinn Robertson                                         |
 * CSE 002                                                 |
 * Homework 03                                             |
 * Converting the average rainfall over an area to         |
 * cubic miles based on user input                         |
 * --------------------------------------------------------|
 */
import java.util.Scanner;
public class Convert {
  // Creating main method
  public static void main(String[] args){
    // Declaring variables
    // Creating scanner
    Scanner scnr = new Scanner( System.in );
    // Constants
    final double ACRE_INCH_TO_GALLONS = 27154.0; // One acre inch of rainfall is 27,154 gallons
    final double GALLONS_TO_CUBIC_FEET = 0.133681; // One gallon is .133681 cubic feet
    final double CUBIC_MILES_IN_CUBIC_FEET = Math.pow(5280, 3.0); // Cubic feet in a cubic mile
    
    // Declaring output variables
    double userAcres;
    double userRainfall;
    double userAcreInches;
    double rainfallInGallons;
    double gallonsInCubicFeet;
    double cubicFeetInCubicMiles;
    
    // Prompts for user input
    System.out.print("Enter affected area in acres: ");
    userAcres = scnr.nextDouble();
    System.out.print("Enter rainfall in affected area: ");
    userRainfall = scnr.nextDouble();
    
    // Calculations
    userAcreInches = userRainfall * userAcres; // Input to acre inches
    rainfallInGallons = userAcreInches * ACRE_INCH_TO_GALLONS; // Acre inches to gallons
    gallonsInCubicFeet = rainfallInGallons * GALLONS_TO_CUBIC_FEET; // Gallons to cubic feet
    cubicFeetInCubicMiles = gallonsInCubicFeet / CUBIC_MILES_IN_CUBIC_FEET; // Cubic feet to cubic miles
    
    System.out.println("" +  cubicFeetInCubicMiles + " cubic miles affected");
    
  } // End of main method
} // End of class