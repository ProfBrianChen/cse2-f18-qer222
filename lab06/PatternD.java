/*--------------------------------------------
 * Quinn Robertson
 * CSE 002
 * Lab 06
 * Pattern printing
 */
import java.util.Scanner;

public class PatternD{
  // Main method
  public static void main(String[] args){
    Scanner scnr = new Scanner( System.in );
    int lineNumber = 0;
    boolean intSentinal = false;
    
    // Getting an int, checking it's an int
    while (!intSentinal){
      System.out.print("Please enter what number you would like to go to: ");
      intSentinal = scnr.hasNextInt();
      if(intSentinal){
        lineNumber = scnr.nextInt();
        System.out.println();
        if(lineNumber > 10 || lineNumber < 1){
          intSentinal = false;
          System.out.println("Please enter a number between 1-10");
        }
      }
      else{
        System.out.println("Please enter an integer.");
        scnr.next();
      }
    }
    
    // Printing
    for(int i = lineNumber; i >= 1; --i){
      for(int j = i; j >= 1; --j){
        System.out.print(j + " ");
      }
      System.out.println();
    }
    
  }
}