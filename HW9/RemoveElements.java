/*-----------------
 * Quinn Robertson
 * CSE 002-110
 * HW09
 * Working with arrays and methods
 */
import java.util.Scanner;
public class RemoveElements {
	// MY CODE
	// Random int[] creation method
	public static int[] randomInput(){
		int[] output = new int[10];
		for(int i = 0; i < 10; ++i){
			output[i] = (int)(Math.random() * 10);
		}
		return output;
	}
	// Deleting method
	public static int[] delete(int[] inputs, int pos){
		// Checking pos in range
		if(pos < 0 || pos >= inputs.length){
			System.out.println("Not in range");
			return inputs;
		}
		// Copying inputs, without inputs[pos]
		int[] newIn = new int[inputs.length - 1];
		for(int i = 0; i < pos; ++i){
			newIn[i] = inputs[i];
		}
		for(int i = pos; i < inputs.length-1; ++i){
			newIn[i] = inputs[i+1];
		}
		return newIn;
	}

	// Removing method
	public static int[] remove(int[] inputs, int num){
		// Copying finding what points the element is in
		boolean done = false;
		int[] finalList = new int[inputs.length];
		for(int i = 0; i < inputs.length; ++i){
			finalList[i] = inputs[i];
		}
		while(!done){
			int pos = -1;
			for(int i = 0; i < finalList.length; ++i){
				if(finalList[i] == num){
					pos = i;
				}
			}
			if(pos == -1){
				if(finalList.length == inputs.length){
					System.out.println("Element "+num+" was not found");
				}
				return finalList;
			}
			else{
				int[] listIter = delete(finalList, pos);
				finalList = listIter;
			}
		}
		int[] error = {-1};
		return error;
	}
	// END MY CODE
	// PROF CARR CODE :::::::::::::::::::::::::::::::::::::::::::
    public static void main(String [] arg){
		Scanner scan=new Scanner(System.in);
		int num[]=new int[10];
		int newArray1[];
		int newArray2[];
		int index,target;
		String answer="";
		do{
			System.out.println("Random input 10 ints [0-9]");
			num = randomInput();
			String out = "The original array is:";
			out += listArray(num);
			System.out.println(out);
		
			System.out.print("Enter the index ");
			index = scan.nextInt();
			newArray1 = delete(num,index);
			String out1="The output array is ";
			out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
			System.out.println(out1);
		
			System.out.print("Enter the target value ");
			target = scan.nextInt();
			newArray2 = remove(num,target);
			String out2="The output array is ";
			out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
			System.out.println(out2);
			
			System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
			answer=scan.next();
		}while(answer.equals("Y") || answer.equals("y"));
	}
	
	public static String listArray(int num[]){
		String out="{";
		for(int j=0;j<num.length;j++){
		if(j>0){
			out+=", ";
		}
		out+=num[j];
		}
		out+="} ";
		return out;
	}
	// END PROF CARR :::::::::::::::::::::::::::::::::::::::::::::
}