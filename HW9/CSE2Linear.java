/*-----------------
 * Quinn Robertson
 * CSE 002-110
 * HW09
 * Working with arrays and linear and binary search methods
 */
import java.util.Scanner;
import java.util.Arrays;
public class CSE2Linear {
	// Basic print method
	public static void print(int[] input){
		System.out.print("[");
		for(int i = 0; i < input.length - 1; ++i){
			System.out.print(input[i] + ", ");
		}
		System.out.println(input[input.length-1]+"]");
	}

	// Scrambling by taking the first element and swapping it with a random one
	public static void scrambler(int[] input){
		for(int i = 0; i < 71; ++i){
			int rn = (int)(Math.random() * (input.length));
			int temp = input[0];
			input[0] = input[rn];
			input[rn] = temp;
		}
	}

	// Linear search method
	public static int[] LinearSearch(Scanner scnr, int[] inputs){
		// Creating an error search
		int search = -1;
		System.out.print("Enter a number to search for: ");
		boolean sentinal = false;

		// Making sure search is an int to look for
		while(!sentinal){
			if(!scnr.hasNextInt()){
				System.out.println("Enter an int");
				scnr.next();
			}
			else{
				sentinal = scnr.hasNextInt();
				search = scnr.nextInt();
			}
		}
		
		// Iterating through the elements
		for(int i = 0; i < inputs.length; ++i){
			if(inputs[i] == search){
				int[] output = {i, i+1, search};
				return output;
			}
		}
		int[] output = {-1, inputs.length, search};
		return output;
	}

	// Binary search method
	public static int[] BinarySearch(Scanner scnr, int[] inputs){
		// Declaring high, low, and mid
		int high = inputs.length-1;
		int low = 0;
		int mid = (high + low)/2;
		int search = -1;
		System.out.print("Enter a number to search for: ");
		boolean sentinal = false;

		// Making sure search is an int
		while(!sentinal){
			if(!scnr.hasNextInt()){
				System.out.println("Enter an int");
				scnr.next();
			}
			else{
				sentinal = scnr.hasNextInt();
				search = scnr.nextInt();
			}
		}
		int counter = 1;
		// Binary searching through elements
		while(mid > low && mid < high){
			if(search == inputs[high]){
				int[] output = {high, counter, search};
				return output;
			}
			else if(search == inputs[low]){
				int[] output = {low, counter, search};
				return output;
			}
			else if(search == inputs[mid]){
				int[] output = {mid, counter, search};
				return output;
			}
			else if(search < inputs[mid]){
				high = mid;
				low += 1;
				mid = (low + high)/2;
				counter = counter + 1;
			}
			else if(search > inputs[mid]){
				low = mid;
				high -= 1;
				mid = (low + high)/2;
				counter = counter + 1;
			}
		}
		int[] error = {-1, counter, search};
		return error;
	}

  	public static void main(String[] args) {
		Scanner scnr = new Scanner( System.in );
    	int[] grades = new int[15];
		boolean sentinal = false;
		System.out.println("Enter 15 ascending ints for grades: ");
		int i = 0;
		while(i < grades.length){
			if(scnr.hasNextInt()){
				int x = scnr.nextInt();
				if(i == 0 || grades[i-1] <= x){
					if(x > 100 || x < 0){
						System.out.println("Enter integers 0-100. Re-enter");
					}
					else{
						grades[i] = x;	
					}

				}
				else{
					System.out.println("Enter ascending integers. Re-enter");
					i = 0;
				}
			}
			else{
				System.out.println("Enter integers. Re-enter");
				i = 0;
			}
			++i;
		}

		// Printing then Binary search
		print(grades);
		int[] searchReturn = BinarySearch(scnr, grades);

		// Printing
		if(searchReturn[0] == -1){
			System.out.println(searchReturn[2] + " was not found in the array in " + searchReturn[1] + " searches.");
		}
		else{
			System.out.println(searchReturn[2] + " was found in the array in " + searchReturn[1] + " searches.");
		}

		// Scrambler
		scrambler(grades);
		System.out.println("Scrambled:");
		print(grades);

		// Searching then printing
		int[] searchReturn2 = LinearSearch(scnr, grades);
		if(searchReturn2[0] == -1){
			System.out.println(searchReturn2[2] + " was not found in the array in " + searchReturn2[1] + " searches.");
		}
		else{
			System.out.println(searchReturn2[2] + " was found in the array in " + searchReturn2[1] + " searches.");
		}
  	}
}