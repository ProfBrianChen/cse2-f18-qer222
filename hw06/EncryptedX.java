/*------------------
 * Quinn Robertson |
 * Homework 06     |
 * Create an X with|
 * specifications  |
 *------------------
 */
import java.util.*;

public class EncryptedX{
  public static void main(String[] args){
    // Getting scanner
    Scanner scnr = new Scanner( System.in );

    // Declaring side of square variable and int sentinal
    int squareSideSize = 0;
    boolean isInt;

    // Prompting user
    do{
      System.out.print("Side length of square (0-100): ");
      isInt = scnr.hasNextInt();
      if(isInt){
        squareSideSize = scnr.nextInt();
        if(squareSideSize >= 101 || squareSideSize < 0){
          System.out.println("Enter a number between 0 and 100");
          isInt = false;
        }
      }
      else{
        System.out.println("Enter an integer");
        scnr.next();
      }
    } while(!isInt);

    // Creating the square with the X
    for(int i = 0; i <= squareSideSize; ++i){
      for(int j = 1; j <= squareSideSize+1; ++j){
        if(i+1 == j || i == squareSideSize-j+1){
          System.out.print(" ");
        }
        else{
          System.out.print("*");
        }
      }
      System.out.println("");
    }
  }
}
