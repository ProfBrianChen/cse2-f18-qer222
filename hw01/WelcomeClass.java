/*
 *
 *  Quinn Robertson
 *  CSE 002 Welcome Class
 *
 */

public class WelcomeClass{
  
  public static void main(String[] args){
    // Printing statements
    /*
    Prints:
      -----------
      | WELCOME |
      -----------
      ^  ^  ^  ^  ^  ^
     / \/ \/ \/ \/ \/ \
    <-Q--E--R--2--2--2->
     \ /\ /\ /\ /\ /\ /
      v  v  v  v  v  v

    */
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-Q--E--R--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    
    
  }
  
}