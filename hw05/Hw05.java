/*------------------
 * Quinn Robertson |
 * Homework 05     |
 * Poker hand      |
 * probabilities   |
 *------------------
 * Calculate       |
 * probabilities of|
 * poker hands     |
 *------------------
 */
import java.util.*;

public class Hw05{
  public static void main(String[] args){
    // Gathering scanner
    Scanner scnr = new Scanner( System.in );
    // Declaring variables
    int cardOne = 0, cardTwo = 0, cardThree = 0, cardFour = 0, cardFive = 0;
    // Declaring suit and number variables
    int cardOneNumber, cardOneSuit;
    int cardTwoNumber, cardTwoSuit;
    int cardThreeNumber, cardThreeSuit;
    int cardFourNumber, cardFourSuit;
    int cardFiveNumber, cardFiveSuit;

    // Creating sentinal
    boolean correctInt = false;

    // Creating counters
    int iterations = 100;
    int onePair = 0;
    int twoPair = 0 ;
    int threeOfAKind = 0;
    int fourofAKind = 0;


    while (!correctInt) {
      System.out.print("Enter number of iterations to calculate: ");
      correctInt = scnr.hasNextInt();
      if (correctInt) {
        System.out.println();
        iterations = scnr.nextInt();
      }
      else {
        System.out.println();
        scnr.next();
        System.out.println("Try again.");
      }
    }

    for (int i=0; i < iterations; ++i) {
      cardOne = (int)(Math.random()*52 + 1);
      do {
        cardTwo = (int)(Math.random()*52 + 1);
      } while (cardOne == cardTwo);
      do {
        cardThree = (int)(Math.random()*52 + 1);
      } while (cardOne == cardThree || cardTwo == cardThree);
      do {
        cardFour = (int)(Math.random()*52 + 1);
      } while (cardOne == cardFour || cardTwo == cardFour || cardThree == cardFour);
      do {
        cardFive = (int)(Math.random()*52 + 1);
      } while (cardOne == cardFive || cardTwo == cardFive || cardThree == cardFive || cardFour == cardFive);

      // Card number/suit definitions
      cardOneSuit = (cardOne % 4) + 1;
      cardOneNumber = (cardOne % 13) + 1;
      cardTwoSuit = (cardTwo % 4) + 1;
      cardTwoNumber = (cardTwo % 13) + 1;
      cardThreeSuit = (cardThree % 4) + 1;
      cardThreeNumber = (cardThree % 13) + 1;
      cardFourSuit = (cardFour % 4) + 1;
      cardFourNumber = (cardFour % 13) + 1;
      cardFiveSuit = (cardFive % 4) + 1;
      cardFiveNumber = (cardFive % 13) + 1;

      // Declaring bools for card comparisons
      boolean cards12, cards13, cards14, cards15;
      boolean cards23, cards24, cards25;
      boolean cards34, cards35;
      boolean cards45;
      cards12 = cardOneNumber == cardTwoNumber;
      cards13 = cardOneNumber == cardThreeNumber;
      cards14 = cardOneNumber == cardFourNumber;
      cards15 = cardOneNumber == cardFiveNumber;
      cards23 = cardTwoNumber == cardThreeNumber;
      cards24 = cardTwoNumber == cardThreeNumber;
      cards25 = cardTwoNumber == cardFiveNumber;
      cards34 = cardThreeNumber == cardFourNumber;
      cards35 = cardThreeNumber == cardFiveNumber;
      cards45 = cardFourNumber == cardFiveNumber;

      // Comparisons determining the type of hand in booleans
      boolean onePairBool, twoPairBool, threeOfAKindBool, fourofAKindBool;
      onePairBool = ((cards12 && !cards23 && !cards24 && !cards25)||(cards13 && !cards23 && !cards34 && !cards35)||(cards14 && !cards24 && !cards34 && !cards45)||(cards15 && !cards25 && !cards35 && !cards45)||(cards23 && !cards13 && !cards34 && !cards35)||(cards24 && !cards14 && !cards34 && !cards45)||(cards25 && !cards15 && !cards35 && !cards45)||(cards34 && !cards14 && !cards24 && !cards45)||(cards35 && !cards15 && !cards25 && !cards45)||(cards45 && !cards15 && !cards25 && !cards35));

      twoPairBool = ((cards12 && cards34 && !cards13 && !cards15 && !cards35)||(cards12 && cards35 && !cards13 && !cards14 && !cards34)||(cards12 && cards45 && !cards14 && !cards13 && !cards34)||(cards13 && cards45 && !cards14 && !cards12 && !cards24)||(cards23 && cards45 && !cards24 && !cards12 && !cards14)||(cards13 && cards24 && !cards12 && !cards15 && !cards25)||(cards13 && cards25 && !cards12 && !cards14 && !cards24)||(cards14 && cards25 && !cards12 && !cards13 && !cards35)||(cards14 && cards35 && !cards12 && !cards13 && !cards23)||(cards24 && cards35 && !cards23 && !cards12 && !cards13)||(cards14 && cards23 && !cards12 && !cards15 && !cards25)||(cards15 && cards23 && !cards12 && !cards14 && !cards24)||(cards15 && cards24 && !cards12 && !cards13 && !cards23)||(cards15 && cards34 && !cards13 && !cards12 && !cards23)||(cards25 && cards34 && !cards23 && !cards12 && !cards13));

      threeOfAKindBool = ((cards12 && cards23 && !cards24 && !cards25)||(cards13 && !cards23 && cards34 && !cards35)||(cards14 && cards24 && !cards34 && !cards45)||(cards15 && cards25 && !cards35 && !cards45)||(cards23 && !cards13 && cards34 && !cards35)||(cards24 && !cards14 && !cards34 && cards45)||(cards25 && !cards15 && cards35 && !cards45)||(cards34 && !cards14 && !cards24 && cards45)||(cards35 && cards15 && !cards25 && !cards45)||(cards45 && cards15 && !cards25 && !cards35));

      fourofAKindBool = ((cards12 && cards23 && cards34 && !cards45)||(cards12 && cards23 && cards35 && !cards34)||(cards12 && cards24 && cards45 && !cards23)||(cards13 && cards34 && cards45 && !cards12)||(cards23 && cards34 && cards45 && !cards12));

      if (fourofAKindBool){
        ++fourofAKind;
      }
      else if (threeOfAKindBool){
        ++threeOfAKind;
      }
      else if (twoPairBool){
        ++twoPair;
      }
      else if (onePairBool){
        ++onePair;
      }
    }

    // Final calculations
    double onePairProb = ((double)(onePair)) / iterations;
    double twoPairProb = ((double)(twoPair)) / iterations;
    double threeOfAKindProb = ((double)(threeOfAKind)) / iterations;
    double fourofAKindProb = ((double)(fourofAKind)) / iterations;

    // Final printout
    System.out.printf("Number of loop: %d", iterations);
    System.out.printf("Probability of Four-of-a-kind: %.3f \n", fourofAKindProb);
    System.out.printf("Probability of Three-of-a-kind: %.3f \n", threeOfAKindProb);
    System.out.printf("Probability of Two Pair: %.3f \n", twoPairProb);
    System.out.printf("Probability of One Pair: %.3f \n", onePairProb);
  }
}