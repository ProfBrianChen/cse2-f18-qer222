/*
 * CSE 002 Hello World 
 */

public class HelloWorld{
  
  public static void main(String[] args){
    // Prints Hello, World to terminal
    System.out.println("Hello, World");
    
  }
  
}