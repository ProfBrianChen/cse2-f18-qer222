/*--------------------------------------------
 * Quinn Robertson
 * CSE 002
 * Lab 07
 * Generating random sentnces
 */

import java.util.*;
public class Methods{
  public static String Adjective(){
    String str = "";
    int choice = (int) Math.floor(Math.random() * Math.floor(10));
    switch(choice){
      case 0: str = "fluffy";
      case 1: str = "yellow";
      case 2: str = "hairy";
      case 3: str = "gross";
      case 4: str = "big";
      case 5: str = "hilarious";
      case 6: str = "tired";
      case 7: str = "toxic";
      case 8: str = "hungry";
      case 9: str = "random";
    }
    return str;
  }
  public static String SubjectNoun(){
    String str = "";
    int choice = (int) Math.floor(Math.random() * Math.floor(10));
    switch(choice){
      case 0: str = "frog";
      case 1: str = "game";
      case 2: str = "rifle";
      case 3: str = "bandage";
      case 4: str = "mountain";
      case 5: str = "computer";
      case 6: str = "centipede";
      case 7: str = "yo-yo";
      case 8: str = "water";
      case 9: str = "metal";
    }
    return str;
  }
  public static String Pronouns(){
    String str = "";
    int choice = (int) Math.floor(Math.random() * Math.floor(3));
    switch(choice){
      case 0: str = "He";
      case 1: str = "She";
      case 2: str = "It";
    }
    return str;
  }
  public static String RegularNoun(){
    String str = "";
    int choice = (int) Math.floor(Math.random() * Math.floor(10));
    switch(choice){
      case 0: str = "chopstick";
      case 1: str = "food";
      case 2: str = "bomb";
      case 3: str = "book";
      case 4: str = "charger";
      case 5: str = "phone";
      case 6: str = "table";
      case 7: str = "tank";
      case 8: str = "wallet";
      case 9: str = "tissue";
    }
    return str;
  }
  public static String Adverb(){
    String str = "";
    int choice = (int) Math.floor(Math.random() * Math.floor(10));
    switch(choice){
      case 0: str = "quickly";
      case 1: str = "slowly";
      case 2: str = "stupidly";
      case 3: str = "angrily";
      case 4: str = "hastily";
      case 5: str = "forcefully";
      case 6: str = "sloppily";
      case 7: str = "meekly";
      case 8: str = "worryingly";
      case 9: str = "matter-of-factly";
    }
    return str;
  }
  public static String Verbs(){
    String str = "";
    int choice = (int) Math.floor(Math.random() * Math.floor(10));
    switch(choice){
      case 0: str = "throw";
      case 1: str = "killed";
      case 2: str = "pin";
      case 3: str = "ran over";
      case 4: str = "hit";
      case 5: str = "jumped on";
      case 6: str = "stapled";
      case 7: str = "ate";
      case 8: str = "destroyed";
      case 9: str = "drank";
    }
    return str;
  }
  
  public static String Thesis(String subject){
    String str = "The "+ Adjective() + " " + Adjective() + " " + subject + " " + Verbs() + " the " + Adjective() + " " + RegularNoun() + ". ";
    return str;
  }
  
  public static String ActionSentences(String pronoun){
    String str = pronoun + " used " + RegularNoun() + " to " + Verbs() + " " + RegularNoun() + " at the " + Adjective() + " " + RegularNoun() + ". ";
    return str;
  }
  
  public static String Conclusion(String subject, String pronoun){
    String str = "That " + subject + " " + Verbs() + " her " + RegularNoun() + "!";
    return str;
  }
  public static String OneSentence(){
    String noun = SubjectNoun();
    return Thesis(noun);
  }
  public static void Paragraph(){
    String noun = SubjectNoun();
    String pronoun = Pronouns();
    System.out.println(Thesis(noun));
    System.out.println(ActionSentences(pronoun));
    System.out.println(ActionSentences(pronoun));
    System.out.println(ActionSentences(pronoun));
    System.out.println(Conclusion(noun, pronoun));
  }
  
  public static void main(String[] args){
    Scanner scnr = new Scanner( System.in );
    boolean sentinal = false;
    int choice = 0;
    do{
      System.out.print("Would you like one sentence, or a paragraph? (1 for one sentence, 2 for paragraph): ");
      sentinal = scnr.hasNextInt();
      if(sentinal){
        choice = scnr.nextInt();
      }
      else{
        System.out.println("Please enter an int");
        scnr.next();
      }
      if(choice != 1 && choice != 2 && sentinal){
        System.out.println("Please enter a choice");
        scnr.next();
        sentinal = false;
      }
    }while(!sentinal);
    while(sentinal){
      if(choice == 1){
        System.out.println(OneSentence());
        System.out.println("Would you like to print another? (y for yes)");
        String choiceSent = scnr.next();
        if(choiceSent.equals("y")){
          sentinal = true;
        }
        else{
          sentinal = false;
        }
      }
      else{
        Paragraph();
        sentinal = false;
      }
    }
  }
}