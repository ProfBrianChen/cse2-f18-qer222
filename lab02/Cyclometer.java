/*--------------------------------------------
 * Quinn Robertson
 * CSE 002
 * Lab 02
 * Cyclometer: Counts the number of rotations of a wheel,
 * which is used to calculate the distance of a trip. 
 */

public class Cyclometer {
        // main method required for every Java program
       public static void main(String[] args) {
         // Creating our input data used later
         int secsTrip1=480; // The seconds passed during trip 1
         int secsTrip2=3220; // The seconds passed during trip 2
         int countsTrip1=1561; // The number of rotations of the wheel in trip 1
         int countsTrip2=9037; // The number of rotations of the wheel in trip 2
         
         // Useful constants for calculations
         double wheelDiameter=27.0; // The diameter of the wheel of the car
         double PI=3.14159; // A double for Pi
         double feetPerMile=5280; // An int for feet in a mile
         double inchesPerFoot=12; // An int for inches per foot
         double secondsPerMinute=60; // An int for the number of seconds in a minute
         
         // End result variables being declared
         double distanceTrip1, distanceTrip2, totalDistance; // Doubles for distance of trip 1 and 2 and the total distance driven
         
         // Printing calculations for the trips
         System.out.println("Trip 1 took "+((double)secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");
         System.out.println("Trip 2 took "+((double)secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");
         
         distanceTrip1 = countsTrip1 * wheelDiameter * PI;
         /* Above gives distance in inches
          * (for every count, the wheel diameter
          * times pi is traveled in inches)
          * for trip 1
          */
         distanceTrip1/=inchesPerFoot*feetPerMile; // Converts distance to miles for trip 1
         distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // Does both previous steps at the same time for trip 2
         
         totalDistance=distanceTrip1+distanceTrip2; // Adds the distances
         
         // Printing the output data to screen
         System.out.println("Trip 1 was "+distanceTrip1+" miles");
         System.out.println("Trip 2 was "+distanceTrip2+" miles");
         System.out.println("The total distance was "+totalDistance+" miles");

         
    }  //end of main method   
} //end of class