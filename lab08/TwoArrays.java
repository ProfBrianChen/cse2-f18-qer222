/*--------------------------------------------
 * Quinn Robertson
 * CSE 002
 * Lab 08 
 * Working with arrays and counting numbers
 */
import java.util.*;
public class TwoArrays{
  public static void main(String[] args){
    // Declaring each array and giving them a size
    int[] ints = new int[100];
    int[] numbOfEachInt = new int[100];
    // Giving the array its values
    for(int i = 0; i <= 99; ++i){
      ints[i] = (int) Math.floor(Math.random() * 100);
    }
    
    // Counting the number of times for each integer
    for(int k = 0; k <= 99; ++k){
      int counter = 0;
      
      for(int j = 0; j <= 99; ++j){
        if(k == ints[j]){
          ++counter;
        }
      }
      numbOfEachInt[k] = counter;
    }
    
    // Printing out the statements
    for(int i = 0; i <= 99; ++i){
      if(numbOfEachInt[i] != 0){
        System.out.println("" + i + " occurs " + numbOfEachInt[i] + " times");
      }
    }
  }
}