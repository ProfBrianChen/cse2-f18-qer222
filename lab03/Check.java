/*--------------------------------------------
 * Quinn Robertson
 * CSE 002
 * Lab 03
 * Calculate the different proportions of a
 * check between friends at dinner for percent tip
 */

// Importations
import java.util.Scanner;

public class Check {
  // Main method
  public static void main(String[] args){
    // Creating Scanner
    Scanner myScanner = new Scanner( System.in );
    
    // Prompting the user for value of check
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    
    // Accepting input
    double checkCost = myScanner.nextDouble();
    
    // Prompting user for percent
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    
    // Accepting input
    double tipPercent = myScanner.nextDouble();
    
    tipPercent /= 100; // Converting the percent to a decimal
    
    // Prompting for the number of users
    System.out.print("Enter the number of people who went out to dinner: ");
    
    // Accepting input
    int numPeople = myScanner.nextInt();
    
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies; //whole dollar amount of cost, as well as how much change in ints to the right decimal place
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    
    // Get the full amount, dropping decimal fraction
    dollars=(int)costPerPerson;
    // Getting the amount of dimes needed
    dimes=(int)(costPerPerson * 10) % 10;
    pennies=(int)(costPerPerson * 100) % 10;
    
    // Printing to user
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);


  } // End of main method
  
} // End of class
