/*--------------------------------------------
 * Quinn Robertson
 * CSE 002
 * Lab 02
 * Used to generate a random card
 * from a number generator
 */

public class CardGenerator{
  // Creating main method
  public static void main(String[] args){
    // Creating variables
    int randomNum, cardNumIdent, cardSuitIdent;
    String cardNum = "";
    String cardSuit = "";
    
    // Generating random number between 1 and 52
    randomNum = (int)(Math.random()*(52))+1;
    
    // Modding the number to give card suit
    cardNumIdent = randomNum % 13;
    cardSuitIdent = randomNum % 4;
    
    // Giving the card its string number value
    if (cardNumIdent <= 10 && cardNumIdent != 0){
      if (cardNumIdent == 1){
        cardNum = "Ace";
      }
      else
      {
        cardNum = String.valueOf(cardNumIdent);
      }
    }
    else
    {
      switch (cardNumIdent){
        case 11: cardNum = "Jack";
          break;
        case 12: cardNum = "Queen";
          break;
        case 0: cardNum = "King";
          break;
      }
    }
    
    // Getting the card suit
    switch (cardSuitIdent) {
      case 0: cardSuit = "Spades";
        break;
      case 1: cardSuit = "Hearts";
        break;
      case 2: cardSuit = "Diamonds";
        break;
      case 3: cardSuit = "Clubs";
        break;
    }
    
    // Outputting the statement
    System.out.println("You picked the " + cardNum + " of " + cardSuit);
    
  } // End of main method
} // End of class